import React, { Component } from 'react';
import Quiz from './Quiz';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: null,
      type: null
    };
  }
  componentDidMount() {
    const questions = require('./questions.json');
    this.setState({ questions: questions });
  }

  selectType(type) {
    this.setState({ type: type });
  }

  render() {
    const { questions, type } = this.state;
    return (
      <div className='App'>
        <header className='App-header'>
          <h1>Dobrodošli u sfero kviz!</h1>
          <p>Izaberite grupu pitanja</p>
          <div>
            <button
              className='btn btn-primary'
              type='button'
              onClick={() => this.selectType(1)}
            >
              5. Razred
            </button>
            <button
              className='btn btn-primary'
              type='button'
              onClick={() => this.selectType(2)}
            >
              6. Razred
            </button>
            <button
              className='btn btn-primary'
              type='button'
              onClick={() => this.selectType(3)}
            >
              7. Razred
            </button>
            <button
              className='btn btn-primary'
              type='button'
              onClick={() => this.selectType(4)}
            >
              8. Razred
            </button>
          </div>
        </header>
        <Quiz questions={questions} type={type} />
      </div>
    );
  }
}

export default App;
