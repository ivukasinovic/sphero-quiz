import React, { Component } from 'react';

export default class Quiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: null,
      answers: []
    };
  }

  isSelected(target) {
    const { answers } = this.state;
    return answers.find(answer => answer === target);
  }

  selectAnswer(answer) {
    const { answers } = this.state;
    let answersTemp = answers;
    const finded = answersTemp.find(target => answer === target);

    const sameQuestion = answersTemp.find(target => {
      return target.toString()[0] === answer.toString()[0];
    });
    if (finded) {
      answersTemp = answersTemp.filter(target => target !== answer);
    } else if (sameQuestion) {
      answersTemp = answersTemp.filter(target => target !== sameQuestion);
      answersTemp.push(answer);
    } else {
      answersTemp.push(answer);
    }

    this.setState({ answers: answersTemp });
  }

  renderQuestions(type) {
    const { questions } = this.props;
    const { answers } = this.state;
    if (questions) {
      return (
        <div>
          <br />
          <div className='container'>
            <h2>1.{questions[type].question1.title}</h2>
            <ul className='nav-pills nav-fill'>
              <li
                className={`nav-item ${
                  this.isSelected(11) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(11)}
              >
                {questions[type].question1.answer1}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(12) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(12)}
              >
                {questions[type].question1.answer2}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(13) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(13)}
              >
                {questions[type].question1.answer3}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(14) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(14)}
              >
                {questions[type].question1.answer4}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(15) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(15)}
              >
                {questions[type].question1.answer5}
              </li>
            </ul>
            <br />
            <h2>2.{questions[type].question2.title}</h2>
            <ul className='nav-pills nav-fill'>
              <li
                className={`nav-item ${
                  this.isSelected(21) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(21)}
              >
                {questions[type].question2.answer1}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(22) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(22)}
              >
                {questions[type].question2.answer2}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(23) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(23)}
              >
                {questions[type].question2.answer3}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(24) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(24)}
              >
                {questions[type].question2.answer4}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(25) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(25)}
              >
                {questions[type].question2.answer5}
              </li>
            </ul>
            <br />
            <h2>3.{questions[type].question3.title}</h2>
            <ul className='nav-pills nav-fill'>
              <li
                className={`nav-item ${
                  this.isSelected(31) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(31)}
              >
                {questions[type].question3.answer1}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(32) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(32)}
              >
                {questions[type].question3.answer2}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(33) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(33)}
              >
                {questions[type].question3.answer3}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(34) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(34)}
              >
                {questions[type].question3.answer4}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(35) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(35)}
              >
                {questions[type].question3.answer5}
              </li>
            </ul>
            <h2>4.{questions[type].question4.title}</h2>
            <ul className='nav-pills nav-fill'>
              <li
                className={`nav-item ${
                  this.isSelected(41) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(41)}
              >
                {questions[type].question4.answer1}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(42) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(42)}
              >
                {questions[type].question4.answer2}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(43) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(43)}
              >
                {questions[type].question4.answer3}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(44) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(44)}
              >
                {questions[type].question4.answer4}
              </li>
              <li
                className={`nav-item ${
                  this.isSelected(45) ? 'nav-link active' : ''
                }`}
                onClick={() => this.selectAnswer(45)}
              >
                {questions[type].question4.answer5}
              </li>
            </ul>
            <br />
          </div>
          <form
            action='https://formspree.io/fdigitalna@gmail.com'
            method='POST'
          >
            <input
              type='text'
              name='type'
              value={type}
              onChange={() => {}}
              hidden
            />
            <input
              type='text'
              name='answers'
              value={answers.sort()}
              onChange={() => {}}
              hidden
            />
            <input
              className='btn btn-success'
              type='submit'
              value='Završi kviz'
            />
          </form>
        </div>
      );
    }
  }

  render() {
    const { type, questions } = this.props;
    return <div>{questions && type ? this.renderQuestions(type) : ''}</div>;
  }
}
